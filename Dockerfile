FROM php:7.2-apache

RUN apt-get update && apt-get install -y

RUN docker-php-ext-install mysqli pdo_mysql

RUN mkdir /app \
 && mkdir /app/solution \
 && mkdir /app/solution/www

COPY www/ /app/solution/www/

RUN cp -r /app/solution/www/* /var/www/html/.